# C# GraphConceptsLibrary

## Introduction
This project aims to convert C# source code to Conceptual Graphs (described further down). This library uses the Roslyn A.K.A .Net Compiler to get source code's AST (Abstract Syntax Tree) and a more abstract graph (Conceptual Graph) is generated from the obtained AST.

## Installation
You only need the [.dll executable](https://gitlab.com/aris_project/CSharp_GraphConceptsLibrary/blob/master/GraphConceptLibrary/GraphConceptLibrary/bin/Debug/GraphConceptsLibrary.dll) and reference it in your [visual studio](https://visualstudio.microsoft.com/downloads/) project. This library was developed for [.Net Core](https://www.microsoft.com/net/download), so it is usable in Windows, Linux and MacOS.

## Development
If you wish to fork the project and work on it you would need visual studio, for C# projects [visual studio](https://visualstudio.microsoft.com/downloads/) is available on Windows, MacOS and Linux. You just need to double click on the .sln file and the project will be setup on your machine with visual studio.

## Usage

### Quick and easy use
This Library is used in the [CSharp2csv project](https://gitlab.com/aris_project/CSharp2csv) which you can download the [CSharp2csv executable](https://gitlab.com/aris_project/CSharp2csv/blob/master/CSharp2CSV/bin/Debug/netcoreapp2.0/CSharp2CSV.dll) instead of developing your own code or use it as a reference.

## Conceptual Graphs

**Relation Types**
- **Condition:** Conditional statement within an If clause
- **Contains:** A concept that contains or depends on another concept
- **Defines:** Block that gives a definition of a namespace
- **Depends:** Block that depends other namespaces
- **Parameter:** A parameter in a method definition
- **Returns:** Return statement of a method

**Concept Types**
- **AssignOp:** An assignment of a value to a field or variable
- **Block:** Set of concepts that are logically grouped together (e.g., code that is inside {…})
- **Class:** Declaration or definition of a class
- **CompareOp:** Binary comparison (e.g. >=, ==, !=, etc.)
- **Enum:** Declaration of an enumerated set of values
- **Field:** Declaration of a variable in a class
- **If:** A conditional branching statement
- **LogicalOp:** Binary logical operation (e.g. &&, ||, etc.)
- **Loop:** Iterative process that depends on a condition
- **MathOp:** Mathematical operation (e.g. `*`,+,-, etc.)
- **Method:** Declaration or definition of a function part of a class
- **Method-Call:** Method invocation
- **Namespace:** Defines a scope that can contain one or more classes
- **Null:** Null reference
- **String:** Constant textual entity
- **Switch:** Conditional statement that has multiple branches
- **Try-Catch:** Try block followed by catch clauses
- **Variable:** Entity declared in an implementation that holds values during execution

In a Conceptual Graph a node is concept and an edge is a relation.

### Example

Input C# code.
```CSharp
public static int Summation(int n) 
{ 
    public static int Summation(int n) 
    { 
        int sum = 0; 

        for (int i = 0; i <= n; i++) 
        { 
            sum += i; 
        } 
        return sum;
    } 
} 
```
Output Conceptual Graph.

![Factorial Graph](./documentation/images/for_sigma.png)

*Note: The graph begins at the "Block: Root" Node (on the right hand side).

