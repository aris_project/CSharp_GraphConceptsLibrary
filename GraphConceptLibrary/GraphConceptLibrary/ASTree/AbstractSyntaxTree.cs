﻿using Roslyn.Compilers.CSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * Author: Daniela Grijincu 
 * daniela.grijincu.2013@nuim.ie
 * 
 * */
namespace GraphConceptsLibrary.ASTree
{
    public class AbstractSyntaxTree
    {

        public SyntaxTree Tree { get; set; }
        public string FileName { get; set; }

        public AbstractSyntaxTree() { }

        public AbstractSyntaxTree(String filename)
        {
            loadASTFromSourceFile(filename);
        }

        public void loadASTFromSourceFile(String filename)
        {
            try
            {
                Tree = Roslyn.Compilers.CSharp.SyntaxTree.ParseFile(filename);
                FileName = filename.Substring(filename.LastIndexOf('\\') + 1);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + ex.StackTrace);
            }
        }

        public void loadASTFromInputTextString(String sourcecode)
        {
            try
            {
                Tree = Roslyn.Compilers.CSharp.SyntaxTree.ParseText(sourcecode);
                FileName = "Root";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + ex.StackTrace);
            }

        }

    }
}
