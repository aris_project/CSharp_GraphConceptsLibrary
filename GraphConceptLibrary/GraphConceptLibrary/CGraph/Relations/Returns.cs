﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * Author: Daniela Grijincu 
 * daniela.grijincu.2013@nuim.ie
 * 
 * */
namespace GraphConceptsLibrary.CGraph.Relations
{
    public class Returns : GraphConcept
    {
        public String InConcept { get; set; }
        public String OutConcept { get; set; }

        public Returns(String name) : base("Returns: " + name, true) { }
        public Returns() : base("Returns", true) { }
       
        public Returns(String inconcept, String outconcept)
            : base("Returns", true)
        {
            InConcept = inconcept;
            OutConcept = outconcept;
        }
    }
}
