﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * Author: Daniela Grijincu 
 * daniela.grijincu.2013@nuim.ie
 * 
 * */
namespace GraphConceptsLibrary.CGraph.Relations
{
    public class Depends : GraphConcept
    {
        public String InConcept { get; set; }
        public String OutConcept { get; set; }

        public Depends(String name) : base(name, true) { }
        public Depends() : base("Depends", true) { }

        public Depends(String inconcept, String outconcept)
            : base("Depends", true)
        {
            InConcept = inconcept;
            OutConcept = outconcept;
        }
    }
}
