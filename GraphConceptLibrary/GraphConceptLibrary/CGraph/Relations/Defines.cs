﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * Author: Daniela Grijincu 
 * daniela.grijincu.2013@nuim.ie
 * 
 * */
namespace GraphConceptsLibrary.CGraph.Relations
{
    public class Defines : GraphConcept
    {
        public String InConcept { get; set; }
        public String OutConcept { get; set; }

        public Defines() : base("Defines", true) { }

        public Defines(String inconcept, String outconcept)
            : base("Defines", true)
        {
            InConcept = inconcept;
            OutConcept = outconcept;
        }
    }
}
