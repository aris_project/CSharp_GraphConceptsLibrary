﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * Author: Daniela Grijincu 
 * daniela.grijincu.2013@nuim.ie
 * 
 * */
namespace GraphConceptsLibrary.CGraph.Relations
{
    public class Condition : GraphConcept
    {
        public String InConcept { get; set; }
        public String OutConcept { get; set; }

        public Condition() : base("Condition", true) { }
        public Condition(String inconcept, String outconcept)
            : base("Condition", true)
        {
            InConcept = inconcept;
            OutConcept = outconcept;
        }
    }
}
