﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * Author: Daniela Grijincu 
 * daniela.grijincu.2013@nuim.ie
 * 
 * */
namespace GraphConceptsLibrary.CGraph.Relations
{
    public class Contains : GraphConcept
    {
        public String InConcept { get; set; }
        public String OutConcept { get; set; }

        public Contains() : base("Contains", true) { }
        public Contains(String inconcept, String outconcept)
            : base("Contains", true)
        {
            InConcept = inconcept;
            OutConcept = outconcept;
        }
    }
}
