﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * Author: Daniela Grijincu 
 * daniela.grijincu.2013@nuim.ie
 * 
 * */
namespace GraphConceptsLibrary.CGraph.Relations
{
    public class Parameter : GraphConcept
    {
        public String InConcept { get; set; }
        public String OutConcept { get; set; }

        public Parameter(String name) : base(name, true) { }
        public Parameter() : base("Parameter", true) { }

        public Parameter(String inconcept, String outconcept)
            : base("Parameter", true)
        {
            InConcept = inconcept;
            OutConcept = outconcept;
        }
    }
}
