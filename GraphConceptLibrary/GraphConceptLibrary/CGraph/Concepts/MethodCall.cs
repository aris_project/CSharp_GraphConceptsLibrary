﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * Author: Daniela Grijincu 
 * daniela.grijincu.2013@nuim.ie
 * 
 * */
namespace GraphConceptsLibrary.CGraph.Concepts
{
    public class MethodCall : Action
    {
        public String Arguments { get; set; }
        public MethodCall(String name) : base("MethodCall: " + name) { }
    }
}
