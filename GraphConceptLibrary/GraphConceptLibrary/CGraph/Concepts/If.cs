﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * Author: Daniela Grijincu 
 * daniela.grijincu.2013@nuim.ie
 * 
 * */
namespace GraphConceptsLibrary.CGraph.Concepts
{
    public class If : GraphConcept
    {
        public If(String name, String value) : base("If:* " + name, value, false) { }
        public If(String name) : base("If" + name, false) { }
        public If() : base("If", false) { }
    }
}
