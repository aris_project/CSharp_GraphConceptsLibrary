﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * Author: Daniela Grijincu 
 * daniela.grijincu.2013@nuim.ie
 * 
 * */
namespace GraphConceptsLibrary.CGraph.Concepts
{
    public class MathOp : Action
    {
        public String OP { get; set; }

        public MathOp(String op) : base("MathOp: " + op) { OP = op; }
    }
}
