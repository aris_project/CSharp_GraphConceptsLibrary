﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphConceptsLibrary.CGraph.Concepts
{
    class Conditional : GraphConcept
    {
        public Conditional() : base("Conditional", false) { }
    }
}
