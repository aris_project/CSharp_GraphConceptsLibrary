﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * Author: Daniela Grijincu 
 * daniela.grijincu.2013@nuim.ie
 * 
 * */
namespace GraphConceptsLibrary.CGraph.Concepts
{
    public class Loop : GraphConcept
    {
        public String Type { get; set; }

        public String Condition { get; set; }

        public Loop(String name, String value) : base("Loop:" + name, value, false) { }
        public Loop(String name) : base("Loop:" + name, false) { }
        public Loop() : base("Loop", false) { }
    }
}
