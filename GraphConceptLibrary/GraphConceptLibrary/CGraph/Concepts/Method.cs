﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * Author: Daniela Grijincu 
 * daniela.grijincu.2013@nuim.ie
 * 
 * */
namespace GraphConceptsLibrary.CGraph.Concepts
{
    public class Method : GraphConcept
    {
        public String ReturnType { get; set; }
        public String Modifiers { get; set; }
        public String ParameterList { get; set; }

        public Method(String name, String value) : base("Method: " + name, value, false) { }
    }
}
