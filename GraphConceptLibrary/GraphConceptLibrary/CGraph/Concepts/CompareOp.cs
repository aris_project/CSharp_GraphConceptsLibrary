﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/**
 * Author: Daniela Grijincu 
 * daniela.grijincu.2013@nuim.ie
 * 
 * */
namespace GraphConceptsLibrary.CGraph.Concepts
{
    public class CompareOp : Action
    {
        public String OP { get; set; }

        public CompareOp(String op) : base("CompareOp: " + op) { OP = op; }
    }
}
