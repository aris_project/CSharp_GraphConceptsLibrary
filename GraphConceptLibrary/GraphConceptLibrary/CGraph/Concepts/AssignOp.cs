﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * Author: Daniela Grijincu 
 * daniela.grijincu.2013@nuim.ie
 * 
 * */
namespace GraphConceptsLibrary.CGraph.Concepts
{
    public class AssignOp : Action
    {
        public String LeftSide { get; set; }
        public String RightSide { get; set; }
        public AssignOp() : base("Assign:*") { }
    }
}
