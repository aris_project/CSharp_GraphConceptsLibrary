﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * Author: Daniela Grijincu 
 * daniela.grijincu.2013@nuim.ie
 * 
 * */
namespace GraphConceptsLibrary.CGraph.Concepts
{
    public class Block : GraphConcept
    {
        public Block(String name, String value) : base("Block: " + name, value, false) { }
        public Block() : base("Block", false) { }

    }
}
