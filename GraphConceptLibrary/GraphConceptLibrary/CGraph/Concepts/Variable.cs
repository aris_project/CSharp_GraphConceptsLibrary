﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * Author: Daniela Grijincu 
 * daniela.grijincu.2013@nuim.ie
 * 
 * */
namespace GraphConceptsLibrary.CGraph.Concepts
{
    public class Variable : GraphConcept
    {
        public String Type { get; set; }
        public String Scope { get; set; }

        public Variable(String name, String value) : base("Variable: " + name, value, false) { }
    }
}
