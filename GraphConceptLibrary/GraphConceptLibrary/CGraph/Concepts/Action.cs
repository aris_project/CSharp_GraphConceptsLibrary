﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * Author: Daniela Grijincu 
 * daniela.grijincu.2013@nuim.ie
 * 
 * */
namespace GraphConceptsLibrary.CGraph.Concepts
{
    public class Action : GraphConcept
    {
        public Action() : base("", false) { }
        public Action(String name, String value) : base(name, value, false) { }
        public Action(String name) : base(name, false) { }
    }
}
