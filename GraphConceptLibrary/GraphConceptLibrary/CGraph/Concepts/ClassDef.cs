﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/**
 * Author: Daniela Grijincu 
 * daniela.grijincu.2013@nuim.ie
 * 
 * */
namespace GraphConceptsLibrary.CGraph.Concepts
{
    public class ClassDef : GraphConcept
    {
        public ClassDef(String name, String value) : base("Class: " + name, value, false) { }
    }
}
