﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * Author: Daniela Grijincu 
 * daniela.grijincu.2013@nuim.ie
 * 
 * */
namespace GraphConceptsLibrary.CGraph.Concepts
{
    public class Field : GraphConcept
    {
        public String Modifiers { get; set; }
        public String Type { get; set; }
        public String Scope { get; set; }

        public Field(String name, String value) : base("Field: " + name, value, false) { }


    }
}
