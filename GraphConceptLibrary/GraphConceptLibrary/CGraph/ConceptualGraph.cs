﻿using GraphConceptsLibrary.ASTree;
using GraphConceptsLibrary.CGraph.Concepts;
using GraphConceptsLibrary.CGraph.Relations;
using QuickGraph;
using QuickGraph.Algorithms;
using Roslyn.Compilers.CSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

/**
 * Author: Daniela Grijincu 
 * daniela.grijincu.2013@nuim.ie
 * Last modified by Felicia Halim
 * Email : FELICIA.HALIM.2014@nuim.ie
 * */
namespace GraphConceptsLibrary.CGraph
{
    public class ConceptualGraph : BidirectionalGraph<GraphConcept, GraphEdge>
    {
        public double ConvergeValue { get; set; }

        public GraphConcept Root { get; set; }

        public ConceptualGraph()
            : base(false)
        {
        }

        private void addVertex(GraphConcept concept)
        {
            if (concept != null)
                this.AddVertex(concept);
        }

        private void addEdge(GraphConcept source, GraphConcept target)
        {
            if (source != null && target != null)
                this.AddEdge(new GraphEdge(source, target));
        }

        private void addVerticesAndEdge(GraphConcept source, GraphConcept target)
        {
            if (source != null && target != null)
                this.AddVerticesAndEdge(new GraphEdge(source, target));
        }

        public static ConceptualGraph loadGraphFromString(String code)
        {
            AbstractSyntaxTree tree = new AbstractSyntaxTree();
            tree.loadASTFromInputTextString(code);

            ConceptualGraph graph = new ConceptualGraph();
            graph.loadGraphFromAST(tree);


            return graph;
        }

        public static List<T> getConceptsByType<T>(ConceptualGraph graph) where T : GraphConcept
        {
            return graph.Vertices.OfType<T>().ToList();
        }

        public void loadGraphFromAST(AbstractSyntaxTree ast)
        {
            SyntaxNode root = ast.Tree.GetRoot();

            Root = new Block(ast.FileName, root.GetText().ToString());
            this.addVertex(Root);
            try
            {

                foreach (var node in root.ChildNodes().OfType<SyntaxNode>())
                {
                    switch (node.Kind)
                    {
                        case SyntaxKind.UsingDirective:
                            {
                                // depends on namespace
                                Namespace ns = new Namespace(((UsingDirectiveSyntax)node).Name.ToString(), node.GetText().ToString());
                                Depends depends = new Depends(Root.Name, ns.Name);
                                this.addVerticesAndEdge(depends, ns);
                                this.addEdge(Root, depends);
                                break;
                            }

                        case SyntaxKind.EnumDeclaration:
                            {
                                // contains enum
                                EnumDef en = new EnumDef(((EnumDeclarationSyntax)node).Identifier.ToString(), node.GetText().ToString());
                                Contains contains = new Contains(Root.Name, en.Name);
                                this.addVerticesAndEdge(en, new StringDef(((EnumDeclarationSyntax)node).Members.ToString(), ""));
                                this.addVerticesAndEdge(contains, en);
                                this.addEdge(Root, contains);
                                break;
                            }

                        case SyntaxKind.FieldDeclaration:
                            {
                                //class contains FieldDeclaration
                                try
                                {
                                    Field field1 = createFieldFromFieldDeclaration((FieldDeclarationSyntax)node, Root);

                                    // class contains field
                                    if (field1 != null)
                                    {
                                        Contains fieldContains = new Contains(Root.Name, field1.Name);
                                        this.addVerticesAndEdge(fieldContains, field1);
                                        this.addVerticesAndEdge(Root, fieldContains);
                                    }

                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.Message + ex.StackTrace);
                                    throw ex;
                                }

                                break;
                            }

                        case SyntaxKind.MethodDeclaration:
                            {
                                try
                                {
                                    Method method = createMethodFromMethodDeclaration((MethodDeclarationSyntax)node);

                                    // class contains Method
                                    Contains contains = new Contains(Root.Name, method.Name);
                                    this.addVerticesAndEdge(contains, method);
                                    this.addEdge(Root, contains);

                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.Message + ex.StackTrace);
                                }
                                break;
                            }

                        case SyntaxKind.ClassDeclaration:
                            {
                                try
                                {
                                    ClassDef classDef = createClassDefFromClassDeclaration((ClassDeclarationSyntax)node);

                                    // namespace contains class
                                    Contains contains = new Contains(Root.Name, classDef.Name);
                                    this.addVerticesAndEdge(contains, classDef);
                                    this.addEdge(Root, contains);
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.Message + ex.StackTrace);
                                }
                                break;

                            }

                        case SyntaxKind.NamespaceDeclaration:
                            {
                                //defines namespace
                                Namespace fileNamespace = new Namespace(((NamespaceDeclarationSyntax)node).Name.ToString(), node.GetText().ToString());
                                Defines defines = new Defines(Root.Name, fileNamespace.Name);

                                this.addVerticesAndEdge(defines, fileNamespace);
                                this.addEdge(Root, defines);

                                foreach (var nsNode in node.ChildNodes().OfType<SyntaxNode>())
                                {
                                    switch (nsNode.Kind)
                                    {
                                        case SyntaxKind.ClassDeclaration:
                                            {
                                                try
                                                {
                                                    ClassDef classDef = createClassDefFromClassDeclaration((ClassDeclarationSyntax)nsNode);

                                                    // namespace contains class
                                                    Contains contains = new Contains(fileNamespace.Name, classDef.Name);
                                                    this.addVerticesAndEdge(contains, classDef);
                                                    this.addVerticesAndEdge(fileNamespace, contains);
                                                }
                                                catch (Exception ex)
                                                {
                                                    Console.WriteLine(ex.Message + ex.StackTrace);
                                                }
                                                break;

                                            }

                                        case SyntaxKind.FieldDeclaration:
                                            {
                                                //class contains FieldDeclaration
                                                try
                                                {
                                                    Field field1 = createFieldFromFieldDeclaration((FieldDeclarationSyntax)nsNode, fileNamespace);

                                                    // class contains field
                                                    if (field1 != null)
                                                    {
                                                        Contains fieldContains = new Contains(fileNamespace.Name, field1.Name);
                                                        this.addVerticesAndEdge(fieldContains, field1);
                                                        this.addVerticesAndEdge(fileNamespace, fieldContains);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    //Console.WriteLine(ex.Message + ex.StackTrace);
                                                }

                                                break;
                                            }

                                        case SyntaxKind.MethodDeclaration:
                                            {
                                                try
                                                {
                                                    Method method = createMethodFromMethodDeclaration((MethodDeclarationSyntax)nsNode);

                                                    // class contains Method
                                                    Contains classContains = new Contains(fileNamespace.Name, method.Name);
                                                    this.addVerticesAndEdge(classContains, method);
                                                    this.addVerticesAndEdge(fileNamespace, classContains);

                                                }
                                                catch (Exception ex)
                                                {
                                                    Console.WriteLine(ex.Message + ex.StackTrace);
                                                }
                                                break;
                                            }

                                        case SyntaxKind.EnumDeclaration:
                                            {
                                                // contains enum
                                                EnumDef en = new EnumDef(((EnumDeclarationSyntax)nsNode).Identifier.ToString(), nsNode.GetText().ToString());

                                                Contains contains = new Contains(fileNamespace.Name, en.Name);
                                                this.addVerticesAndEdge(en, new StringDef(((EnumDeclarationSyntax)nsNode).Members.ToString(), ""));
                                                this.addVerticesAndEdge(contains, en);
                                                this.addVerticesAndEdge(fileNamespace, contains);
                                                break;
                                            }

                                        default: break;
                                    } // switch namespace childs
                                }
                                break; // case namespace
                            } // namespace

                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }

            // Calculate NODE RANKS for every VERTICE
            GraphMetrics.calculateActualNodeRanks(this);
        }

        private ClassDef createClassDefFromClassDeclaration(ClassDeclarationSyntax nsNode)
        {
            ClassDef classDef = new ClassDef(nsNode.Identifier.ToString(), nsNode.GetText().ToString());

            foreach (var classNode in nsNode.ChildNodes().OfType<SyntaxNode>())
            {
                switch (classNode.Kind)
                {
                    case SyntaxKind.FieldDeclaration:
                        {
                            //class contains FieldDeclaration
                            try
                            {
                                Field field1 = createFieldFromFieldDeclaration((FieldDeclarationSyntax)classNode, classDef);

                                // class contains field
                                Contains fieldContains = new Contains(classDef.Name, field1.Name);
                                this.addVerticesAndEdge(fieldContains, field1);
                                this.addVerticesAndEdge(classDef, fieldContains);

                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message + ex.StackTrace);
                            }


                            break;
                        }

                    case SyntaxKind.MethodDeclaration:
                        {
                            try
                            {
                                Method method = createMethodFromMethodDeclaration((MethodDeclarationSyntax)classNode);

                                if (method != null)
                                {
                                    // class contains Method
                                    Contains classContains = new Contains(classDef.Name, method.Name);
                                    this.addVerticesAndEdge(classContains, method);
                                    this.addVerticesAndEdge(classDef, classContains);
                                }

                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message + ex.StackTrace);
                            }
                            break;
                        }

                    case SyntaxKind.ConstructorDeclaration:
                        {
                            try
                            {
                                Method method = createMethodFromMethodDeclaration((ConstructorDeclarationSyntax)classNode);

                                if (method != null)
                                {
                                    // class contains Method
                                    Contains classContains = new Contains(classDef.Name, method.Name);
                                    this.addVerticesAndEdge(classContains, method);
                                    this.addVerticesAndEdge(classDef, classContains);
                                }

                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message + ex.StackTrace);
                            }
                            break;
                        }

                    case SyntaxKind.PropertyDeclaration:
                        {
                            break;
                        }

                    default: break;
                }
            }

            return classDef;
        }

        private Field createFieldFromFieldDeclaration(FieldDeclarationSyntax classNode, GraphConcept parentNode)
        {
            var vars = classNode.DescendantNodes().OfType<VariableDeclaratorSyntax>();

            if (vars != null && vars.Count() > 0)
            {

                VariableDeclaratorSyntax varb = vars.ElementAt(0);


                Field field1 = new Field(varb.Identifier.ToString(), classNode.GetText().ToString());
                field1.Modifiers = classNode.Modifiers.ToString();
                field1.Type = ((VariableDeclarationSyntax)varb.Parent).Type.ToString();
                field1.Scope = parentNode.Name;

                if (varb.Initializer != null)
                {
                    // field is declared + initialized
                    // class contains assign* -> contains field

                    var eqResult = classNode.DescendantNodes().OfType<EqualsValueClauseSyntax>();

                    if (eqResult != null && eqResult.Count() > 0)
                    {
                        EqualsValueClauseSyntax equals = eqResult.ElementAt(0);

                        AssignOp assign = createAssignOpFromEquals(equals, parentNode.Name);

                        if (assign != null)
                        {
                            // assign contains also the field on the left side
                            Contains contains1 = new Contains(assign.Name, field1.Name);
                            this.addVerticesAndEdge(contains1, field1);
                            this.addVerticesAndEdge(assign, contains1);

                            Contains contains2 = new Contains(parentNode.Name, assign.Name);
                            this.addVerticesAndEdge(contains2, assign);
                            this.addVerticesAndEdge(parentNode, contains2);
                        }
                    }
                }

                return field1;
            }

            return null;

        }

        private Method createMethodFromMethodDeclaration(SyntaxNode classNode)
        {
            Method method = null;
            if (classNode.GetType() == typeof(MethodDeclarationSyntax))
            {
                method = new Method(((MethodDeclarationSyntax)classNode).Identifier.ToString(), classNode.GetText().ToString());
                method.ParameterList = ((MethodDeclarationSyntax)classNode).ParameterList.ToString();
                method.Modifiers = ((MethodDeclarationSyntax)classNode).Modifiers.ToString();
                method.ReturnType = ((MethodDeclarationSyntax)classNode).ReturnType.ToString();

                // method has input parameters? => add Parameter relations
                var param = ((MethodDeclarationSyntax)classNode).ParameterList.ChildNodes().OfType<ParameterSyntax>();
                foreach (ParameterSyntax p in param)
                {
                    Variable v = new Variable(p.Identifier.ToString(), p.GetText().ToString());
                    v.Scope = method.Name;
                    v.Type = p.Type.ToString();

                    // relation
                    Parameter pr = new Parameter(method.Name, v.Name);
                    this.addVerticesAndEdge(pr, v);
                    this.addVerticesAndEdge(method, pr);
                }

                // get method implementation => search for BlockSyntax node
                Block methodBlock;
                try
                {
                    BlockSyntax block = ((MethodDeclarationSyntax)classNode).ChildNodes().OfType<BlockSyntax>().First();

                    if (block != null && block.ChildNodes().OfType<SyntaxNode>().Count() > 0) // method has implementation
                    {
                        methodBlock = createBlockFromBlockSyntax(block, method.Name);
                    }
                    else
                    {
                        methodBlock = null;
                    }
                }
                catch (Exception ex)
                {
                    methodBlock = null;
                }

                if (methodBlock != null)
                {
                    Contains containsM = new Contains(method.Name, methodBlock.Name); // method contains block
                    this.addVerticesAndEdge(containsM, methodBlock); // method contains block
                    this.addVerticesAndEdge(method, containsM);
                }

                // method returns a value? => add Returns relation
                if (((MethodDeclarationSyntax)classNode).ReturnType.ToString() != "void")
                {
                    try
                    {
                        var r = ((MethodDeclarationSyntax)classNode).DescendantNodes().OfType<ReturnStatementSyntax>();

                        if (r != null)
                        {
                            foreach (var ret in r)
                            {
                                Returns myreturn2 = (Returns)this.Vertices.SingleOrDefault<GraphConcept>(v => v.IsReturns == true && ((Returns)v).InConcept == method.Name && v.Value == ret.Expression.ToString());
                                if (myreturn2 != null)
                                {
                                    this.addVerticesAndEdge(method, myreturn2);
                                }
                                //added by Felicia 5.9.2014
                                else if (ret.Expression.GetType().Name.ToString() == "ConditionalExpressionSyntax")
                                {

                                    Conditional conditional = createConceptFromConditionalStatement((ConditionalExpressionSyntax)ret.Expression, method.Name);

                                    Returns myreturn = new Returns(method.Name, conditional.Name);
                                    this.addVerticesAndEdge(myreturn, conditional);
                                    this.addVerticesAndEdge(methodBlock, myreturn);
                                }
                                else
                                {
                                    GraphConcept value = createConceptFromExpressionSyntax(ret.Expression, method.Name);
                                    //StringDef value = new StringDef(r.Expression.ToString(), r.GetText().ToString());
                                    Returns myreturn = new Returns(method.Name, value.Name);
                                    this.addVerticesAndEdge(myreturn, value); // method contains block
                                    this.addVerticesAndEdge(method, myreturn);
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        //Console.WriteLine(ex.Message + ex.ToString());
                    }
                }
            }
            else
                if (classNode.GetType() == typeof(ConstructorDeclarationSyntax))
            {
                method = new Method(((ConstructorDeclarationSyntax)classNode).Identifier.ToString(), classNode.GetText().ToString());
                method.ParameterList = ((ConstructorDeclarationSyntax)classNode).ParameterList.ToString();
                method.Modifiers = ((ConstructorDeclarationSyntax)classNode).Modifiers.ToString();

                var param = ((ConstructorDeclarationSyntax)classNode).ParameterList.ChildNodes().OfType<ParameterSyntax>();
                foreach (ParameterSyntax p in param)
                {
                    Variable v = new Variable(p.Identifier.ToString(), p.GetText().ToString());
                    v.Scope = method.Name;
                    v.Type = p.Type.ToString();

                    // relation
                    Parameter pr = new Parameter(method.Name, v.Name);
                    this.addVerticesAndEdge(pr, v);
                    this.addVerticesAndEdge(method, pr);
                }

                // get constructor implementation => search for BlockSyntax node
                Block methodBlock;
                try
                {
                    BlockSyntax block = ((ConstructorDeclarationSyntax)classNode).ChildNodes().OfType<BlockSyntax>().First();
                    if (block != null && block.ChildNodes().OfType<SyntaxNode>().Count() > 0) // method has implementation
                    {
                        methodBlock = createBlockFromBlockSyntax(block, method.Name);
                    }
                    else
                    {
                        methodBlock = null;
                    }
                }
                catch (Exception ex)
                {
                    methodBlock = null;
                }

                if (methodBlock != null)
                {
                    Contains containsM = new Contains(method.Name, methodBlock.Name); // method contains block
                    this.addVerticesAndEdge(containsM, methodBlock); // method contains block
                    this.addVerticesAndEdge(method, containsM);
                }
            }

            return method;
        }

        //recursive function that analyzes a BinaryExpression 
        private MathOp createMathOpFromBinaryExpression(BinaryExpressionSyntax expression, String currentScope)
        {
            var childs = expression.ChildNodes().OfType<SyntaxNode>().ToList();

            var c1 = childs.ElementAt(0);
            var c2 = childs.ElementAt(1);

            if (c1.GetType() == typeof(PrefixUnaryExpressionSyntax) || c1.GetType() == typeof(ParenthesizedExpressionSyntax)
                || c1.GetType() == typeof(CastExpressionSyntax))
            {
                c1 = getSyntaxNodeFromParenthesis(c1);
            }

            if (c2.GetType() == typeof(PrefixUnaryExpressionSyntax) || c2.GetType() == typeof(ParenthesizedExpressionSyntax)
                || c2.GetType() == typeof(CastExpressionSyntax))
            {
                c2 = getSyntaxNodeFromParenthesis(c2);
            }

            MathOp mathop = new MathOp(expression.OperatorToken.ToString());
            mathop.Value = expression.ToString();

            if (c1.GetType() == typeof(BinaryExpressionSyntax))
            {
                MathOp m = createMathOpFromBinaryExpression((BinaryExpressionSyntax)c1, currentScope);
                Contains c = new Contains(mathop.Name, m.Name);
                this.addVerticesAndEdge(c, m);
                this.addVerticesAndEdge(mathop, c);
            }
            else
            {
                if (c1.GetType() == typeof(LiteralExpressionSyntax))
                {
                    StringDef term = new StringDef(((LiteralExpressionSyntax)c1).Token.ToString(), ((LiteralExpressionSyntax)c1).GetText().ToString());
                    Contains c = new Contains(mathop.Name, term.Name);
                    this.addVerticesAndEdge(c, term);
                    this.addVerticesAndEdge(mathop, c);
                }
                else
                    if (c1.GetType() == typeof(IdentifierNameSyntax))
                {
                    GraphConcept concept = searchIdentifierInScope((IdentifierNameSyntax)c1, currentScope);
                    if (concept != null)
                    {
                        Contains c = new Contains(mathop.Name, concept.Name);
                        this.addVerticesAndEdge(c, concept);
                        this.addVerticesAndEdge(mathop, c);
                    }
                    else
                    {
                        // define a null concept. an error has occured as no such variable or field was previously defined
                        NullDef term = new NullDef();
                        Contains c = new Contains(mathop.Name, term.Name);
                        this.addVerticesAndEdge(c, term);
                        this.addVerticesAndEdge(mathop, c);
                    }
                }
                else
                        if (c1.GetType() == typeof(InvocationExpressionSyntax))
                {
                    // creates a MethodCall concept
                    MethodCall method = createMethodCallFromInvocationExpression((InvocationExpressionSyntax)c1, currentScope);
                    Contains c = new Contains(mathop.Name, method.Name);
                    this.addVerticesAndEdge(c, method);
                    this.addVerticesAndEdge(mathop, c);
                }
                else
                            if (c1.GetType() == typeof(TypeOfExpressionSyntax))
                {
                    // creates a MethodCall concept
                    StringDef term = new StringDef("typeof(" + ((TypeOfExpressionSyntax)c1).Type.ToString() + ")", ((TypeOfExpressionSyntax)c1).GetText().ToString());
                    Contains c = new Contains(mathop.Name, term.Name);
                    this.addVerticesAndEdge(c, term);
                    this.addVerticesAndEdge(mathop, c);
                }

            }

            if (c2.GetType() == typeof(BinaryExpressionSyntax))
            {
                MathOp m = createMathOpFromBinaryExpression((BinaryExpressionSyntax)c2, currentScope);
                Contains c = new Contains(mathop.Name, m.Name);
                this.addVerticesAndEdge(c, m);
                this.addVerticesAndEdge(mathop, c);
            }
            else
            {
                if (c2.GetType() == typeof(LiteralExpressionSyntax))
                {
                    StringDef term = new StringDef(((LiteralExpressionSyntax)c2).Token.ToString(), ((LiteralExpressionSyntax)c2).GetText().ToString());
                    Contains c = new Contains(mathop.Name, term.Name);
                    this.addVerticesAndEdge(c, term);
                    this.addVerticesAndEdge(mathop, c);
                }
                else
                    if (c2.GetType() == typeof(IdentifierNameSyntax))
                {
                    GraphConcept concept = searchIdentifierInScope((IdentifierNameSyntax)c2, currentScope);
                    if (concept != null)
                    {
                        Contains c = new Contains(mathop.Name, concept.Name);
                        this.addVerticesAndEdge(c, concept);
                        this.addVerticesAndEdge(mathop, c);
                    }
                    else
                    {
                        // define a null concept. an error has occured as no such variable or field was previously defined
                        NullDef term = new NullDef();
                        Contains c = new Contains(mathop.Name, term.Name);
                        this.addVerticesAndEdge(c, term);
                        this.addVerticesAndEdge(mathop, c);
                    }
                }
                else
                        if (c2.GetType() == typeof(InvocationExpressionSyntax))
                {
                    // creates a MethodCall concept
                    MethodCall method = createMethodCallFromInvocationExpression((InvocationExpressionSyntax)c2, currentScope);
                    Contains c = new Contains(mathop.Name, method.Name);
                    this.addVerticesAndEdge(c, method);
                    this.addVerticesAndEdge(mathop, c);
                }
                else
                            if (c2.GetType() == typeof(TypeOfExpressionSyntax))
                {
                    // creates a MethodCall concept
                    StringDef term = new StringDef("typeof(" + ((TypeOfExpressionSyntax)c2).Type.ToString() + ")", ((TypeOfExpressionSyntax)c2).GetText().ToString());
                    Contains c = new Contains(mathop.Name, term.Name);
                    this.addVerticesAndEdge(c, term);
                    this.addVerticesAndEdge(mathop, c);
                }

            }

            return mathop;

        }

        //recursive function that analyzes a BinaryExpression 
        private CompareOp createCompareOpFromBinaryExpression(BinaryExpressionSyntax expression, String currentScope)
        {
            try
            {
                var childs = expression.ChildNodes().OfType<SyntaxNode>().ToList();

                var c1 = childs.ElementAt(0);
                var c2 = childs.ElementAt(1);

                if (c1.GetType() == typeof(PrefixUnaryExpressionSyntax) || c1.GetType() == typeof(ParenthesizedExpressionSyntax)
                    || c1.GetType() == typeof(CastExpressionSyntax))
                {
                    c1 = getSyntaxNodeFromParenthesis(c1);
                }

                if (c2.GetType() == typeof(PrefixUnaryExpressionSyntax) || c2.GetType() == typeof(ParenthesizedExpressionSyntax)
                    || c2.GetType() == typeof(CastExpressionSyntax))
                {
                    c2 = getSyntaxNodeFromParenthesis(c2);
                }

                CompareOp compareop = new CompareOp(expression.OperatorToken.ToString());
                compareop.Value = expression.ToString();


                if (c1.GetType() == typeof(BinaryExpressionSyntax))
                {
                    MathOp m = createMathOpFromBinaryExpression((BinaryExpressionSyntax)c1, currentScope);
                    Contains c = new Contains(compareop.Name, m.Name);
                    this.addVerticesAndEdge(c, m);
                    this.addVerticesAndEdge(compareop, c);
                }
                else
                {
                    if (c1.GetType() == typeof(LiteralExpressionSyntax))
                    {
                        StringDef term = new StringDef(((LiteralExpressionSyntax)c1).Token.ToString(), ((LiteralExpressionSyntax)c1).GetText().ToString());
                        Contains c = new Contains(compareop.Name, term.Name);
                        this.addVerticesAndEdge(c, term);
                        this.addVerticesAndEdge(compareop, c);
                    }
                    else
                        if (c1.GetType() == typeof(IdentifierNameSyntax))
                    {
                        GraphConcept concept = searchIdentifierInScope((IdentifierNameSyntax)c1, currentScope);
                        if (concept != null)
                        {
                            Contains c = new Contains(compareop.Name, concept.Name);
                            this.addVerticesAndEdge(c, concept);
                            this.addVerticesAndEdge(compareop, c);
                        }
                        else
                        {
                            // define a null concept. an error has occured as no such variable or field was previously defined
                            NullDef term = new NullDef();
                            Contains c = new Contains(compareop.Name, term.Name);
                            this.addVerticesAndEdge(c, term);
                            this.addVerticesAndEdge(compareop, c);
                        }
                    }
                    else
                            if (c1.GetType() == typeof(InvocationExpressionSyntax))
                    {
                        // creates a MethodCall concept
                        MethodCall method = createMethodCallFromInvocationExpression((InvocationExpressionSyntax)c1, currentScope);
                        Contains c = new Contains(compareop.Name, method.Name);
                        this.addVerticesAndEdge(c, method);
                        this.addVerticesAndEdge(compareop, c);
                    }
                    else
                                if (c1.GetType() == typeof(TypeOfExpressionSyntax))
                    {
                        // creates a MethodCall concept
                        StringDef term = new StringDef("typeof(" + ((TypeOfExpressionSyntax)c1).Type.ToString() + ")", ((TypeOfExpressionSyntax)c1).GetText().ToString());
                        Contains c = new Contains(compareop.Name, term.Name);
                        this.addVerticesAndEdge(c, term);
                        this.addVerticesAndEdge(compareop, c);
                    }

                }

                if (c2.GetType() == typeof(BinaryExpressionSyntax))
                {
                    MathOp m = createMathOpFromBinaryExpression((BinaryExpressionSyntax)c2, currentScope);
                    Contains c = new Contains(compareop.Name, m.Name);
                    this.addVerticesAndEdge(c, m);
                    this.addVerticesAndEdge(compareop, c);
                }
                else
                {
                    if (c2.GetType() == typeof(LiteralExpressionSyntax))
                    {
                        StringDef term = new StringDef(((LiteralExpressionSyntax)c2).Token.ToString(), ((LiteralExpressionSyntax)c2).GetText().ToString());
                        Contains c = new Contains(compareop.Name, term.Name);
                        this.addVerticesAndEdge(c, term);
                        this.addVerticesAndEdge(compareop, c);
                    }
                    else
                        if (c2.GetType() == typeof(IdentifierNameSyntax))
                    {
                        GraphConcept concept = searchIdentifierInScope((IdentifierNameSyntax)c2, currentScope);
                        if (concept != null)
                        {
                            Contains c = new Contains(compareop.Name, concept.Name);
                            this.addVerticesAndEdge(c, concept);
                            this.addVerticesAndEdge(compareop, c);
                        }
                        else
                        {
                            // define a null concept. an error has occured as no such variable or field was previously defined
                            NullDef term = new NullDef();
                            Contains c = new Contains(compareop.Name, term.Name);
                            this.addVerticesAndEdge(c, term);
                            this.addVerticesAndEdge(compareop, c);
                        }
                    }
                    else
                            if (c2.GetType() == typeof(InvocationExpressionSyntax))
                    {
                        // creates a MethodCall concept
                        MethodCall method = createMethodCallFromInvocationExpression((InvocationExpressionSyntax)c2, currentScope);
                        Contains c = new Contains(compareop.Name, method.Name);
                        this.addVerticesAndEdge(c, method);
                        this.addVerticesAndEdge(compareop, c);
                    }
                    else
                                if (c2.GetType() == typeof(TypeOfExpressionSyntax))
                    {
                        // creates a MethodCall concept
                        StringDef term = new StringDef("typeof(" + ((TypeOfExpressionSyntax)c2).Type.ToString() + ")", ((TypeOfExpressionSyntax)c2).GetText().ToString());
                        Contains c = new Contains(compareop.Name, term.Name);
                        this.addVerticesAndEdge(c, term);
                        this.addVerticesAndEdge(compareop, c);
                    }

                }

                return compareop;
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message + ex.StackTrace);
            }

            return null;

        }

        private GraphConcept searchIdentifierInScope(IdentifierNameSyntax id, String currentScope)
        {
            try
            {
                // search for variables in the same scope
                var tempVar = from i in this.Vertices.OfType<Variable>()
                              where (i.Name == "Variable: " + id.Identifier.ToString()
                                     && (i as Variable).Scope == currentScope)
                              select i;

                // a variable in the same scope was found
                // if no variable was found, search for a class field
                if (tempVar.Count() == 1)
                {
                    Variable f = tempVar.ElementAt(0);

                    return f;
                }
                else
                {
                    // search for class fields
                    var tempField = from i in this.Vertices.OfType<Field>()
                                    where i.Name == "Field: " + id.Identifier.ToString()
                                    select i;

                    // if a field was found return it, otherwise return null
                    if (tempField.Count() == 1)
                    {
                        Field f = tempField.ElementAt(0);

                        return f;
                    }
                    else
                        return null;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        // analyzes an EqualsValueClauseSyntax construct and adds the corresponding vertices and edges
        private AssignOp createAssignOpFromEquals(EqualsValueClauseSyntax equals, String currentScope)
        {
            // field is declared + initialized
            // class contains assign* -> contains fields
            AssignOp assign = new AssignOp();
            assign.Value = equals.Value.ToString();


            var childs = equals.ChildNodes().OfType<SyntaxNode>().ToList();
            List<SyntaxNode> adds = new List<SyntaxNode>();
            foreach (var c in childs)
            {
                if (c.GetType() == typeof(ParenthesizedExpressionSyntax) || c.GetType() == typeof(PrefixUnaryExpressionSyntax)
                    || c.GetType() == typeof(CastExpressionSyntax))
                {
                    adds.Add(getSyntaxNodeFromParenthesis(c));
                }
            }
            childs.AddRange(adds);

            foreach (var child in childs) //equals.DescendantNodes().OfType<SyntaxNode>())
            {
                if (child.GetType() == typeof(BinaryExpressionSyntax))
                {
                    MathOp mathop = createMathOpFromBinaryExpression((BinaryExpressionSyntax)child, currentScope);

                    Contains c = new Contains(assign.Name, mathop.Name);
                    this.addVerticesAndEdge(c, mathop);
                    this.addVerticesAndEdge(assign, c);
                }
                else
                    if (child.GetType() == typeof(LiteralExpressionSyntax))
                {
                    StringDef term = new StringDef(((LiteralExpressionSyntax)child).Token.ToString(), ((LiteralExpressionSyntax)child).GetText().ToString());
                    Contains c = new Contains(assign.Name, term.Name);
                    this.addVerticesAndEdge(c, term);
                    this.addVerticesAndEdge(assign, c);
                }
                else
                        if (child.GetType() == typeof(IdentifierNameSyntax))
                {
                    GraphConcept concept = searchIdentifierInScope((IdentifierNameSyntax)child, currentScope);
                    if (concept != null)
                    {
                        Contains c = new Contains(assign.Name, concept.Name);
                        this.addVerticesAndEdge(c, concept);
                        this.addVerticesAndEdge(assign, c);
                    }
                    else
                    {
                        // define a null concept. an error has occured as no such variable or field was previously defined
                        NullDef term = new NullDef();
                        Contains c = new Contains(assign.Name, term.Name);
                        this.addVerticesAndEdge(c, term);
                        this.addVerticesAndEdge(assign, c);
                    }
                }
                else
                            if (child.GetType() == typeof(InvocationExpressionSyntax))
                {
                    // creates a MethodCall concept
                    MethodCall method = createMethodCallFromInvocationExpression((InvocationExpressionSyntax)child, currentScope);
                    Contains c = new Contains(assign.Name, method.Name);
                    this.addVerticesAndEdge(c, method);
                    this.addVerticesAndEdge(assign, c);
                }
            }

            return assign;

        }

        private MethodCall createMethodCallFromInvocationExpression(InvocationExpressionSyntax expression, String currentScope)
        {
            MethodCall method = null;
            if (expression.Expression.ToString().StartsWith("this."))
                method = new MethodCall(expression.Expression.ToString().Substring(5) + "()");
            else
                method = new MethodCall(expression.Expression.ToString() + "()");

            method.Arguments = expression.ArgumentList.ToString();
            method.Value = expression.ToString();

            // method has arguments? => add Parameter relations
            var args = expression.ArgumentList.ChildNodes().OfType<ArgumentSyntax>();
            foreach (ArgumentSyntax argument in args)
            {
                var childs = argument.ChildNodes();

                if (childs != null && childs.Count() > 0)
                {
                    SyntaxNode child = childs.ElementAt(0);

                    if (child.GetType() == typeof(LiteralExpressionSyntax))
                    {
                        StringDef term = new StringDef(((LiteralExpressionSyntax)child).Token.ToString(), ((LiteralExpressionSyntax)child).GetText().ToString());

                        Parameter pr = new Parameter(method.Name, term.Name);
                        this.addVerticesAndEdge(pr, term);
                        this.addVerticesAndEdge(method, pr);
                    }
                    else
                        if (child.GetType() == typeof(IdentifierNameSyntax))
                    {
                        GraphConcept concept = searchIdentifierInScope((IdentifierNameSyntax)child, currentScope);
                        if (concept != null)
                        {
                            Parameter pr = new Parameter(method.Name, concept.Name);
                            this.addVerticesAndEdge(pr, concept);
                            this.addVerticesAndEdge(method, pr);
                        }
                        else
                        {
                            // define a null concept. an error has occured as no such variable or field was previously defined
                            NullDef term = new NullDef();
                            Parameter pr = new Parameter(method.Name, term.Name);
                            this.addVerticesAndEdge(pr, concept);
                            this.addVerticesAndEdge(method, pr);
                        }
                    }
                    else

                            if (child.GetType() == typeof(BinaryExpressionSyntax))
                    {
                        MathOp m = createMathOpFromBinaryExpression((BinaryExpressionSyntax)child, currentScope);

                        Parameter pr = new Parameter(method.Name, m.Name);
                        this.addVerticesAndEdge(pr, m);
                        this.addVerticesAndEdge(method, pr);
                    }
                }
            }

            return method;

        }

        private GraphConcept createConceptFromStatementSyntax(StatementSyntax statement, String currentScope)
        {
            switch (statement.Kind)
            {
                case SyntaxKind.VariableDeclarator:
                    {
                        try
                        {
                            VariableDeclaratorSyntax varDecl = (VariableDeclaratorSyntax)statement.DescendantNodes().OfType<VariableDeclaratorSyntax>().FirstOrDefault();

                            if (varDecl != null)
                            {
                                return createConceptFromLocalDeclaration(varDecl, currentScope);
                            }
                        }
                        catch (Exception ex)
                        {
                            //Console.WriteLine(ex.Message + ex.StackTrace);
                        }

                        break;
                    }

                case SyntaxKind.ExpressionStatement:
                    {
                        try
                        {
                            var childs = statement.ChildNodes().OfType<SyntaxNode>();

                            if (childs != null && childs.Count() > 0)
                            {
                                // get Expression
                                var expr = childs.ElementAt(0);

                                if (expr.GetType() == typeof(BinaryExpressionSyntax))
                                {
                                    return createAssignOpFromBinaryExpression((BinaryExpressionSyntax)expr, currentScope);
                                }

                                if (expr.GetType() == typeof(PrefixUnaryExpressionSyntax) || expr.GetType() == typeof(PostfixUnaryExpressionSyntax))
                                {
                                    MathOp mathop;
                                    if (expr.GetType() == typeof(PrefixUnaryExpressionSyntax))
                                        mathop = new MathOp(((PrefixUnaryExpressionSyntax)expr).OperatorToken.ToString());
                                    else
                                        mathop = new MathOp(((PostfixUnaryExpressionSyntax)expr).OperatorToken.ToString());// edited by Felicia 5.28.2014 prefix to postfix

                                    var result = expr.ChildNodes().OfType<IdentifierNameSyntax>();

                                    if (result != null && result.Count() == 1)
                                    {

                                        var id = result.ElementAt(0);

                                        GraphConcept concept = searchIdentifierInScope(id, currentScope);

                                        if (concept != null)
                                        {
                                            Contains c = new Contains(mathop.Name, concept.Name);
                                            this.addVerticesAndEdge(c, concept);
                                            this.addVerticesAndEdge(mathop, c);
                                            return mathop;
                                        }
                                    }

                                    break;
                                }

                                if (expr.GetType() == typeof(InvocationExpressionSyntax))
                                {
                                    return createMethodCallFromInvocationExpression((InvocationExpressionSyntax)expr, currentScope);

                                }

                            }
                        }

                        catch (Exception ex)
                        {
                            //  Console.WriteLine(ex.Message + ex.StackTrace);
                        }

                        break;
                    }

                case SyntaxKind.IfStatement:
                    {
                        try
                        {
                            return createConceptFromIfExpression((IfStatementSyntax)statement, currentScope);
                        }
                        catch (Exception ex)
                        {
                            // Console.WriteLine(ex.Message + ex.StackTrace);
                        }

                        break;
                    }

                case SyntaxKind.WhileStatement:
                    {
                        try
                        {
                            return createLoopConceptFromLoopExpression((WhileStatementSyntax)statement, currentScope);
                        }
                        catch (Exception ex)
                        {
                            // Console.WriteLine(ex.Message + ex.StackTrace);
                        }

                        break;
                    }

                case SyntaxKind.DoStatement:
                    {
                        try
                        {
                            return createLoopConceptFromLoopExpression((DoStatementSyntax)statement, currentScope);
                        }
                        catch (Exception ex)
                        {
                            //Console.WriteLine(ex.Message + ex.StackTrace);
                        }

                        break;
                    }

                case SyntaxKind.ForStatement:
                    {
                        try
                        {
                            return createLoopConceptFromLoopExpression((ForStatementSyntax)statement, currentScope);
                        }
                        catch (Exception ex)
                        {
                            // Console.WriteLine(ex.Message + ex.StackTrace);
                        }

                        break;
                    }

                case SyntaxKind.ForEachStatement:
                    {
                        try
                        {
                            return createLoopConceptFromLoopExpression((ForEachStatementSyntax)statement, currentScope);
                        }
                        catch (Exception ex)
                        {
                            // Console.WriteLine(ex.Message + ex.StackTrace);
                        }

                        break;
                    }

                case SyntaxKind.TryStatement:
                    {
                        try
                        {
                            return createTryStatement((TryStatementSyntax)statement, currentScope);
                        }
                        catch (Exception ex)
                        {
                            // Console.WriteLine(ex.Message + ex.StackTrace);
                        }

                        break;
                    }

                case SyntaxKind.SwitchStatement:
                    {
                        try
                        {
                            return createSwitchStatement((SwitchStatementSyntax)statement, currentScope);
                        }
                        catch (Exception ex)
                        {
                            // Console.WriteLine(ex.Message + ex.StackTrace);
                        }

                        break;
                    }

                case SyntaxKind.ReturnStatement:
                    {

                        try
                        {
                            GraphConcept value = createConceptFromExpressionSyntax(((ReturnStatementSyntax)statement).Expression, currentScope);
                            Returns myreturn = new Returns(currentScope, value.Name);
                            myreturn.Value = ((ReturnStatementSyntax)statement).Expression.ToString();
                            this.addVerticesAndEdge(myreturn, value); // method contains block
                            return myreturn;
                        }
                        catch (Exception ex)
                        {
                            // Console.WriteLine(ex.Message + ex.StackTrace);
                        }

                        break;
                    }


                default: break;
            }

            return null;

        }

        // DEAL WITH BLOCKS {....}
        private Block createBlockFromBlockSyntax(BlockSyntax block, String currentScope)
        {

            // create Block Concept
            Block methodBlock = new Block();

            // traverse through every element in the block and construct the graph
            foreach (var elem in block.ChildNodes().OfType<SyntaxNode>())
            {
                switch (elem.Kind)
                {
                    case SyntaxKind.LocalDeclarationStatement:
                        {
                            try
                            {
                                VariableDeclaratorSyntax varDecl = elem.DescendantNodes().OfType<VariableDeclaratorSyntax>().FirstOrDefault();

                                if (varDecl != null)
                                {
                                    GraphConcept concept = createConceptFromLocalDeclaration(varDecl, currentScope);

                                    if (concept != null)
                                    {
                                        Contains c = new Contains(methodBlock.Name, concept.Name);
                                        this.addVerticesAndEdge(c, concept);
                                        this.addVerticesAndEdge(methodBlock, c);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                //Console.WriteLine(ex.Message + ex.StackTrace);
                            }

                            break;
                        }

                    case SyntaxKind.ExpressionStatement:
                        {
                            try
                            {
                                var childs = elem.ChildNodes().OfType<SyntaxNode>();

                                if (childs != null && childs.Count() > 0)
                                {
                                    // get Expression
                                    var expr = childs.ElementAt(0);

                                    if (expr.GetType() == typeof(BinaryExpressionSyntax))
                                    {
                                        AssignOp assign = createAssignOpFromBinaryExpression((BinaryExpressionSyntax)expr, currentScope);

                                        if (assign != null)
                                        {
                                            Contains c2 = new Contains(methodBlock.Name, assign.Name);
                                            this.addVerticesAndEdge(c2, assign);
                                            this.addVerticesAndEdge(methodBlock, c2);
                                        }

                                        break;
                                    }

                                    if (expr.GetType() == typeof(PrefixUnaryExpressionSyntax) || expr.GetType() == typeof(PostfixUnaryExpressionSyntax))
                                    {
                                        MathOp mathop;
                                        if (expr.GetType() == typeof(PrefixUnaryExpressionSyntax))
                                            mathop = new MathOp(((PrefixUnaryExpressionSyntax)expr).OperatorToken.ToString());
                                        else
                                            mathop = new MathOp(((PostfixUnaryExpressionSyntax)expr).OperatorToken.ToString()); // edited by Felicia 4.30.2014 from PrefixUnaryExpressionSyntax to PostfixUnaryExpressionSyntax

                                        mathop.Value = expr.ToString();
                                        var result = expr.ChildNodes().OfType<IdentifierNameSyntax>();

                                        if (result != null && result.Count() == 1)
                                        {

                                            var id = result.ElementAt(0);

                                            GraphConcept concept = searchIdentifierInScope(id, currentScope);

                                            if (concept != null)
                                            {
                                                Contains c = new Contains(mathop.Name, concept.Name);
                                                this.addVerticesAndEdge(c, concept);
                                                this.addVerticesAndEdge(mathop, c);

                                                Contains c2 = new Contains(methodBlock.Name, mathop.Name);
                                                this.addVerticesAndEdge(c2, mathop);
                                                this.addVerticesAndEdge(methodBlock, c2);
                                            }
                                        }

                                        break;
                                    }

                                    if (expr.GetType() == typeof(InvocationExpressionSyntax))
                                    {

                                        MethodCall method = createMethodCallFromInvocationExpression((InvocationExpressionSyntax)expr, currentScope);

                                        Contains c2 = new Contains(methodBlock.Name, method.Name);
                                        this.addVerticesAndEdge(c2, method);
                                        this.addVerticesAndEdge(methodBlock, c2);

                                        break;
                                    }

                                    //added by Felicia 5.9.2014
                                    if (expr.GetType() == typeof(ConditionalExpressionSyntax))
                                    {

                                        Conditional conditional = createConceptFromConditionalStatement((ConditionalExpressionSyntax)expr, currentScope);

                                        Contains c2 = new Contains(methodBlock.Name, conditional.Name);
                                        this.addVerticesAndEdge(c2, conditional);
                                        this.addVerticesAndEdge(methodBlock, c2);

                                        break;
                                    }

                                }
                            }

                            catch (Exception ex)
                            {
                                //  Console.WriteLine(ex.Message + ex.StackTrace);
                            }

                            break;
                        }

                    case SyntaxKind.IfStatement:
                        {
                            try
                            {
                                GraphConcept ifExpr = createConceptFromIfExpression((IfStatementSyntax)elem, currentScope);
                                if (ifExpr != null)
                                {
                                    Contains c3 = new Contains(methodBlock.Name, ifExpr.Name);
                                    this.addVerticesAndEdge(c3, ifExpr);
                                    this.addVerticesAndEdge(methodBlock, c3);
                                }
                            }
                            catch (Exception ex)
                            {
                                // Console.WriteLine(ex.Message + ex.StackTrace);
                            }

                            break;
                        }

                    case SyntaxKind.WhileStatement:
                        {
                            try
                            {
                                Loop loop = createLoopConceptFromLoopExpression((WhileStatementSyntax)elem, currentScope);
                                if (loop != null)
                                {
                                    loop.Name = "Loop: While";
                                    // method block contains loop
                                    Contains c3 = new Contains(methodBlock.Name, loop.Name);
                                    this.addVerticesAndEdge(c3, loop);
                                    this.addVerticesAndEdge(methodBlock, c3);
                                }
                            }
                            catch (Exception ex)
                            {
                                // Console.WriteLine(ex.Message + ex.StackTrace);
                            }

                            break;
                        }

                    case SyntaxKind.DoStatement:
                        {
                            try
                            {
                                Loop loop = createLoopConceptFromLoopExpression((DoStatementSyntax)elem, currentScope);
                                if (loop != null)
                                {
                                    loop.Name = "Loop: DoWhile";
                                    // method block contains loop
                                    Contains c3 = new Contains(methodBlock.Name, loop.Name);
                                    this.addVerticesAndEdge(c3, loop);
                                    this.addVerticesAndEdge(methodBlock, c3);
                                }
                            }
                            catch (Exception ex)
                            {
                                //Console.WriteLine(ex.Message + ex.StackTrace);
                            }

                            break;
                        }

                    case SyntaxKind.ForStatement:
                        {
                            try
                            {
                                Loop loop = createLoopConceptFromLoopExpression((ForStatementSyntax)elem, currentScope);

                                if (loop != null)
                                {
                                    loop.Name = "Loop: For";
                                    // method block contains loop
                                    Contains c3 = new Contains(methodBlock.Name, loop.Name);
                                    this.addVerticesAndEdge(c3, loop);
                                    this.addVerticesAndEdge(methodBlock, c3);
                                }
                            }
                            catch (Exception ex)
                            {
                                // Console.WriteLine(ex.Message + ex.StackTrace);
                            }

                            break;
                        }

                    case SyntaxKind.ForEachStatement:
                        {
                            try
                            {
                                Loop loop = createLoopConceptFromLoopExpression((ForEachStatementSyntax)elem, currentScope);

                                if (loop != null)
                                {
                                    loop.Name = "Loop: Foreach";
                                    // method block contains loop
                                    Contains c3 = new Contains(methodBlock.Name, loop.Name);
                                    this.addVerticesAndEdge(c3, loop);
                                    this.addVerticesAndEdge(methodBlock, c3);
                                }
                            }
                            catch (Exception ex)
                            {
                                // Console.WriteLine(ex.Message + ex.StackTrace);
                            }

                            break;
                        }

                    case SyntaxKind.TryStatement:
                        {
                            try
                            {
                                TryStatement tryStatement = createTryStatement((TryStatementSyntax)elem, currentScope);

                                if (tryStatement != null)
                                {
                                    tryStatement.Name = "TryCatchStatement: *";
                                    // method block contains loop
                                    Contains c3 = new Contains(methodBlock.Name, tryStatement.Name);
                                    this.addVerticesAndEdge(c3, tryStatement);
                                    this.addVerticesAndEdge(methodBlock, c3);
                                }
                            }
                            catch (Exception ex)
                            {
                                // Console.WriteLine(ex.Message + ex.StackTrace);
                            }

                            break;
                        }

                    case SyntaxKind.SwitchStatement:
                        {
                            try
                            {
                                Switch switchStatement = createSwitchStatement((SwitchStatementSyntax)elem, currentScope);

                                if (switchStatement != null)
                                {
                                    switchStatement.Name = "Switch: *";
                                    // method block contains loop
                                    Contains c3 = new Contains(methodBlock.Name, switchStatement.Name);
                                    this.addVerticesAndEdge(c3, switchStatement);
                                    this.addVerticesAndEdge(methodBlock, c3);
                                }
                            }
                            catch (Exception ex)
                            {
                                // Console.WriteLine(ex.Message + ex.StackTrace);
                            }

                            break;
                        }
                    //added by Felicia 6.12.2014 to handle return statement in a block
                    case SyntaxKind.ReturnStatement:
                        {
                            try
                            {
                                GraphConcept value = createConceptFromExpressionSyntax(((ReturnStatementSyntax)elem).Expression, currentScope);
                                if (value != null)
                                {
                                    Returns myreturn = new Returns(currentScope, value.Name);
                                    myreturn.Value = ((ReturnStatementSyntax)elem).Expression.ToString();
                                    this.addVerticesAndEdge(myreturn, value); // method contains block
                                }
                            }
                            catch (Exception ex)
                            {
                            }
                            break;
                        }


                    default: break;
                }

            }

            return methodBlock;
        }

        private Switch createSwitchStatement(SwitchStatementSyntax switchStatementSyntax, string currentScope)
        {
            try
            {
                Switch mySwitch = new Switch();

                ExpressionSyntax expr = switchStatementSyntax.Expression;
                if (expr != null)
                {
                    GraphConcept switchExpression = createConceptFromExpressionSyntax(expr, currentScope);
                    if (switchExpression != null)
                    {
                        Condition c2 = new Condition(mySwitch.Name, switchExpression.Name);
                        this.addVerticesAndEdge(c2, switchExpression);
                        this.addVerticesAndEdge(mySwitch, c2);
                    }
                }

                SyntaxList<SwitchSectionSyntax> switchSections = switchStatementSyntax.Sections;
                if (switchSections != null)
                {
                    foreach (SwitchSectionSyntax switchSection in switchSections)
                    {
                        SyntaxList<StatementSyntax> statements = switchSection.Statements;
                        foreach (StatementSyntax statement in statements)
                        {
                            GraphConcept concept = createConceptFromStatementSyntax(statement, currentScope);
                            if (concept != null)
                            {
                                Contains cont = new Contains(mySwitch.Name, concept.Name);
                                this.addVerticesAndEdge(cont, concept);
                                this.addVerticesAndEdge(mySwitch, cont);
                            }
                        }

                    }
                }

                return mySwitch;
            }
            catch (Exception ex)
            {
                // Console.WriteLine(ex.Message + ex.StackTrace);
            }

            return null;
        }

        private TryStatement createTryStatement(TryStatementSyntax tryStatementSyntax, string currentScope)
        {
            try
            {
                TryStatement tryStatement = new TryStatement();
                BlockSyntax block = tryStatementSyntax.Block;
                if (block != null)
                {
                    Block tryBlock = createBlockFromBlockSyntax(block, currentScope);
                    if (tryBlock != null)
                    {
                        Contains containsM = new Contains(tryStatement.Name, tryBlock.Name); // try contains block
                        this.addVerticesAndEdge(containsM, tryBlock);
                        this.addVerticesAndEdge(tryStatement, containsM);
                    }
                }

                SyntaxList<CatchClauseSyntax> catches = tryStatementSyntax.Catches;
                if (catches != null)
                {
                    foreach (CatchClauseSyntax catchClause in catches)
                    {
                        BlockSyntax catchBlock = catchClause.Block;
                        if (catchBlock != null)
                        {
                            Block catchTryBlock = createBlockFromBlockSyntax(catchBlock, currentScope);
                            if (catchTryBlock != null)
                            {
                                Contains containsM = new Contains(tryStatement.Name, catchTryBlock.Name);
                                this.addVerticesAndEdge(containsM, catchTryBlock);
                                this.addVerticesAndEdge(tryStatement, containsM);
                            }
                        }
                    }
                }

                return tryStatement;
            }
            catch (Exception ex)
            {
                // Console.WriteLine(ex.Message + ex.StackTrace);
            }
            return null;

        }

        private GraphConcept createConceptFromIfExpression(SyntaxNode ifExpression, String currentScope)
        {
            try
            {
                If myIfExpression = new If();

                // the condition is the first BinaryExpression
                var myexpr = ifExpression.ChildNodes().OfType<BinaryExpressionSyntax>();
                if (myexpr != null && myexpr.Count() > 0)
                {
                    CompareOp compare = createCompareOpFromBinaryExpression(myexpr.ElementAt(0), currentScope);

                    if (compare != null)
                    {
                        Condition c2 = new Condition(myIfExpression.Name, compare.Name);
                        this.addVerticesAndEdge(c2, compare);
                        this.addVerticesAndEdge(myIfExpression, c2);
                    }
                }

                //check if it has a Else statement

                var elseStatement = ifExpression.ChildNodes().OfType<ElseClauseSyntax>().FirstOrDefault();

                // added by Felicia 6.12.2014 to handle elseif statements (can be more than one elseif)
                var elifstatement = (IfStatementSyntax)null;
                if (elseStatement != null)
                {
                    elifstatement = elseStatement.ChildNodes().OfType<IfStatementSyntax>().FirstOrDefault();
                }
                while (elifstatement != null)
                {
                    var elifBlock = elifstatement.ChildNodes().OfType<BlockSyntax>().FirstOrDefault();
                    if (elifBlock != null)
                    {
                        Block loopblock = createBlockFromBlockSyntax(elifBlock, currentScope);
                        Contains c1 = new Contains(myIfExpression.Name, loopblock.Name);
                        this.addVerticesAndEdge(c1, loopblock);
                        this.addVerticesAndEdge(myIfExpression, c1);
                    }
                    else
                    {
                        // this will occur when there is no BlockSyntax node 
                        // if it does not have a block then is has only a single line instruction
                        var exp = (elifstatement).ChildNodes().OfType<ExpressionStatementSyntax>().FirstOrDefault();

                        if (exp != null)
                        {
                            GraphConcept concept = createConceptFromExpressionSyntax(exp, currentScope);

                            if (concept != null)
                            {
                                Contains c1 = new Contains(myIfExpression.Name, concept.Name);
                                this.addVerticesAndEdge(c1, concept);
                                this.addVerticesAndEdge(myIfExpression, c1);
                            }
                        }
                    }
                    elseStatement = elifstatement.ChildNodes().OfType<ElseClauseSyntax>().FirstOrDefault();
                    if (elseStatement != null)
                    {
                        elifstatement = elseStatement.ChildNodes().OfType<IfStatementSyntax>().FirstOrDefault();
                    }
                    else
                    {
                        elifstatement = null;
                    }
                }

                if (elseStatement != null)
                {
                    // check if it has a block
                    var bl = ((ElseClauseSyntax)elseStatement).ChildNodes().OfType<BlockSyntax>().FirstOrDefault();

                    if (bl != null)
                    {
                        Block loopblock = createBlockFromBlockSyntax(bl, currentScope);
                        Contains c1 = new Contains(myIfExpression.Name, loopblock.Name);
                        this.addVerticesAndEdge(c1, loopblock);
                        this.addVerticesAndEdge(myIfExpression, c1);
                    }
                    else
                    {
                        // this will occur when there is no BlockSyntax node 
                        // if it does not have a block then is has only a single line instruction
                        var exp = ((ElseClauseSyntax)elseStatement).ChildNodes().OfType<ExpressionStatementSyntax>().FirstOrDefault();

                        if (exp != null)
                        {
                            GraphConcept concept = createConceptFromExpressionSyntax(exp, currentScope);

                            if (concept != null)
                            {
                                Contains c1 = new Contains(myIfExpression.Name, concept.Name);
                                this.addVerticesAndEdge(c1, concept);
                                this.addVerticesAndEdge(myIfExpression, c1);
                            }
                        }
                    }

                }

                // check if it has a block
                var b = ifExpression.ChildNodes().OfType<BlockSyntax>().FirstOrDefault();

                if (b != null)
                {
                    Block loopblock = createBlockFromBlockSyntax(b, currentScope);
                    Contains c1 = new Contains(myIfExpression.Name, loopblock.Name);
                    this.addVerticesAndEdge(c1, loopblock);
                    this.addVerticesAndEdge(myIfExpression, c1);
                }
                else
                {
                    // this will occur when there is no BlockSyntax node 
                    // if it does not have a block then is has only a single line instruction
                    var exp = ifExpression.ChildNodes().OfType<ExpressionStatementSyntax>().FirstOrDefault();

                    if (exp != null)
                    {
                        GraphConcept concept = createConceptFromExpressionSyntax(exp, currentScope);

                        if (concept != null)
                        {
                            Contains c1 = new Contains(myIfExpression.Name, concept.Name);
                            this.addVerticesAndEdge(c1, concept);
                            this.addVerticesAndEdge(myIfExpression, c1);
                        }
                    }
                    else if (ifExpression.ChildNodes().OfType<ReturnStatementSyntax>().FirstOrDefault() != null)
                    {
                        var returnStatement = ifExpression.ChildNodes().OfType<ReturnStatementSyntax>().FirstOrDefault();
                        GraphConcept value = createConceptFromExpressionSyntax(((ReturnStatementSyntax)returnStatement).Expression, currentScope);
                        Returns myreturn = new Returns(currentScope, value.Name);
                        myreturn.Value = ((ReturnStatementSyntax)returnStatement).Expression.ToString();
                        this.addVerticesAndEdge(myreturn, value);
                    }
                }

                return myIfExpression;


            }
            catch (Exception ex)
            {
                // Console.WriteLine(ex.Message + ex.StackTrace);
            }

            return null;

        }

        private GraphConcept createConceptFromLocalDeclaration(VariableDeclaratorSyntax varDecl, String currentScope)
        {
            try
            {
                Variable variable = new Variable(varDecl.Identifier.ToString(), varDecl.GetText().ToString());
                variable.Scope = currentScope;
                variable.Type = ((VariableDeclarationSyntax)varDecl.Parent).Type.ToString();

                if (varDecl.Initializer == null)
                {
                    // variable is only declared, not initialized
                    // => methodBlock contains variable

                    return variable;

                }
                else
                {
                    // variable is initialized
                    // identifiers on the right side (other variables defined in the class fields)
                    EqualsValueClauseSyntax equals = varDecl.DescendantNodes().OfType<EqualsValueClauseSyntax>().FirstOrDefault();

                    if (equals != null)
                    {
                        AssignOp assign = createAssignOpFromEquals(equals, currentScope);

                        if (assign != null)
                        {
                            Contains c = new Contains(assign.Name, variable.Name);
                            this.addVerticesAndEdge(c, variable);
                            this.addVerticesAndEdge(assign, c);

                            return assign;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Console.WriteLine(ex.Message + ex.StackTrace);
            }

            return null;

        }

        private Loop createLoopConceptFromLoopExpression(SyntaxNode loopExpression, String currentScope)
        {
            try
            {
                //create the loop concept
                string loopExpr = loopExpression.GetText().ToString().Substring(0, loopExpression.GetText().ToString().IndexOf(")") + 1);
                Regex regexDo = new Regex(@"\b" + "do" + @"\b");
                if (regexDo.Match(loopExpr).Success)
                {
                    loopExpr = loopExpression.ToString().Replace(loopExpression.ChildNodes().OfType<BlockSyntax>().FirstOrDefault().Statements.ToString(), "");
                    loopExpr = loopExpr.Replace("do", "").Replace("{", "").Replace("}", "");
                }
                loopExpr = loopExpr.Replace("\r", "").Replace("\n", "").Replace("\t", "").Trim();
                Loop loop = new Loop("", loopExpr);

                // deal with local variable declaration
                var varDeclarations = loopExpression.DescendantNodes().OfType<VariableDeclaratorSyntax>();
                foreach (var v in varDeclarations)
                {
                    GraphConcept concept = createConceptFromLocalDeclaration(v, currentScope);

                    if (concept != null)
                    {
                        Contains c = new Contains(loop.Name, concept.Name);
                        this.addVerticesAndEdge(c, concept);
                        this.addVerticesAndEdge(loop, c);
                    }
                }

                // added by Felicia 4.10.2014 deal with variable in ForEach statement

                if (loopExpression.AncestorsAndSelf().FirstOrDefault().ToString().Contains("foreach"))
                {
                    string varDecl = loopExpression.DescendantNodes().OfType<ArgumentSyntax>().FirstOrDefault().ToString();
                    //Build the variable node

                    String type = loopExpression.DescendantNodes().OfType<PredefinedTypeSyntax>().FirstOrDefault().ToString();
                    SyntaxTree declTree = Roslyn.Compilers.CSharp.SyntaxTree.ParseText(type + " " + varDecl + "= 0");
                    SyntaxNode declNode = declTree.GetRoot();
                    var varDeclarationForEach = declNode.DescendantNodes().OfType<VariableDeclaratorSyntax>().FirstOrDefault();
                    GraphConcept concept = createConceptFromLocalDeclaration(varDeclarationForEach, currentScope);

                    if (concept != null)
                    {
                        Contains c = new Contains(loop.Name, concept.Name);
                        this.addVerticesAndEdge(c, concept);
                        this.addVerticesAndEdge(loop, c);
                    }
                }

                // the condition is the first BinaryExpression
                //commented by Felicia 5.5.2014
                //BinaryExpressionSyntax myexpr = loopExpression.ChildNodes().OfType<BinaryExpressionSyntax>().FirstOrDefault();
                //added by Felicia 5.5.2014 
                BinaryExpressionSyntax myexpr = null;

                //freezed by Felicia 5.5.2014 for foreach loop, no specification for ForEach Loop
                #region for each
                if (loopExpression.AncestorsAndSelf().FirstOrDefault().ToString().Contains("foreach"))
                {
                    string varTemp = "";
                    string varTemp2 = "";

                    foreach (SyntaxNodeOrToken s in loopExpression.ChildNodesAndTokens())
                    {
                        if (s.Kind.ToString().Contains("IdentifierToken"))
                        {
                            varTemp = s.ToString();
                        }
                        if (s.Kind.ToString().Contains("IdentifierName"))
                        {
                            varTemp2 = s.ToString() + ".Length";
                        }
                    }
                    loop.Condition = varTemp + "<" + varTemp2;

                    ExpressionSyntax myExpression = Syntax.ParseExpression(loop.Condition);
                    myexpr = myExpression.AncestorsAndSelf().OfType<BinaryExpressionSyntax>().ToArray()[0];

                }
                #endregion

                // added by Felicia ; loop condition not just the first operator, but the complete loop condition 
                BinaryExpressionSyntax[] loopExpresions = new BinaryExpressionSyntax[100];
                loopExpresions = loopExpression.ChildNodes().OfType<BinaryExpressionSyntax>().ToArray();
                if (loopExpresions != null)
                {
                    if (loopExpresions.Count() >= 2 && loopExpression.AncestorsAndSelf().FirstOrDefault().ToString().Contains("for"))
                    {
                        loop.Condition = loopExpresions[1].OperatorToken.ToString();
                        myexpr = loopExpresions[1];
                    }
                    else if (loopExpresions.Count() == 1 && loopExpression.AncestorsAndSelf().FirstOrDefault().ToString().Contains("for"))
                    {
                        loop.Condition = loopExpresions[0].OperatorToken.ToString();
                        myexpr = loopExpresions[0];
                    }
                    else if (loopExpression.AncestorsAndSelf().FirstOrDefault().ToString().Contains("while"))
                    {
                        loop.Condition = loopExpresions[0].OperatorToken.ToString();
                        myexpr = loopExpresions[0];
                    }

                }

                if (myexpr != null)
                {
                    CompareOp compare = createCompareOpFromBinaryExpression(myexpr, currentScope);

                    if (compare != null)
                    {
                        Condition c2 = new Condition(loop.Name, compare.Name);
                        this.addVerticesAndEdge(c2, compare);
                        this.addVerticesAndEdge(loop, c2);
                    }
                }

                // check if the Loop has a block
                try
                {
                    var b = loopExpression.ChildNodes().OfType<BlockSyntax>().Single();

                    Block loopblock = createBlockFromBlockSyntax(b, currentScope);
                    Contains c1 = new Contains(loop.Name, loopblock.Name);
                    this.addVerticesAndEdge(c1, loopblock);
                    this.addVerticesAndEdge(loop, c1);
                }
                catch (Exception ex)
                {
                    // this exeption will occur when there is no BlockSyntax node in the loop
                    // if it does not have a block then is has only a single line instruction
                    var exp = loopExpression.ChildNodes().OfType<ExpressionStatementSyntax>().FirstOrDefault();

                    if (exp != null)
                    {
                        GraphConcept concept = createConceptFromExpressionSyntax(exp, currentScope);

                        if (concept != null)
                        {
                            Contains c1 = new Contains(loop.Name, concept.Name);
                            this.addVerticesAndEdge(c1, concept);
                            this.addVerticesAndEdge(loop, c1);
                        }
                    }

                }

                // TODO
                // check the third part of the forLoop either a BinaryExpression or a UnaryExpression
                // Rahman added: for updating part of for loop
                if (loopExpression.GetType() == typeof(ForStatementSyntax))
                {
                    ForStatementSyntax forSyntax = (ForStatementSyntax)loopExpression;
                    var incrementors = forSyntax.Incrementors;
                    int size = incrementors.ToArray().Length;
                    ExpressionSyntax[] eArr = new ExpressionSyntax[size];
                    eArr = incrementors.ToArray();

                    for (int i = 0; i < eArr.Length; i++)
                    {
                        GraphConcept concept = createConceptFromExpressionSyntax(eArr[i], currentScope);
                        if (concept != null)
                        {
                            Contains contains = new Contains(loop.Name, concept.Name);
                            this.addVerticesAndEdge(contains, concept);
                            this.addVerticesAndEdge(loop, contains);
                        }
                    }
                }
                return loop;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + ex.StackTrace);
            }
            return null;
        }

        private GraphConcept createConceptFromExpressionSyntax(ExpressionStatementSyntax expression, String currentScope)
        {
            try
            {
                var child = expression.ChildNodes().OfType<SyntaxNode>().ElementAt(0);

                if (child.GetType() == typeof(BinaryExpressionSyntax))
                {
                    MathOp mathop = createMathOpFromBinaryExpression((BinaryExpressionSyntax)child, currentScope);

                    return mathop;
                }
                else
                    if (child.GetType() == typeof(InvocationExpressionSyntax))
                {
                    // creates a MethodCall concept
                    MethodCall method = createMethodCallFromInvocationExpression((InvocationExpressionSyntax)child, currentScope);

                    return method;
                }
                else
                        if (child.GetType() == typeof(PrefixUnaryExpressionSyntax) || child.GetType() == typeof(PostfixUnaryExpressionSyntax))
                {
                    MathOp mathop;
                    if (child.GetType() == typeof(PrefixUnaryExpressionSyntax))
                        mathop = new MathOp(((PrefixUnaryExpressionSyntax)child).OperatorToken.ToString());
                    else
                        mathop = new MathOp(((PostfixUnaryExpressionSyntax)child).OperatorToken.ToString()); //edited by Felicia 5.28.2014 prefix to postfix

                    var id = child.ChildNodes().OfType<IdentifierNameSyntax>().Single();
                    GraphConcept concept = searchIdentifierInScope(id, currentScope);

                    if (concept != null)
                    {
                        Contains c = new Contains(mathop.Name, concept.Name);
                        this.addVerticesAndEdge(c, concept);
                        this.addVerticesAndEdge(mathop, c);
                    }
                    else
                    {
                        // define a null concept. an error has occured as no such variable or field was previously defined
                        NullDef term = new NullDef();
                        Contains c = new Contains(mathop.Name, term.Name);
                        this.addVerticesAndEdge(c, term);
                        this.addVerticesAndEdge(mathop, c);
                    }

                    return mathop;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + ex.StackTrace);
            }

            return null;
        }

        //added by Felicia 5.9.2014
        private Conditional createConceptFromConditionalStatement(ConditionalExpressionSyntax child, String currentScope)
        {
            try
            {
                Conditional conditionalExpression = new Conditional();

                foreach (ExpressionSyntax expr in child.ChildNodes().OfType<ExpressionSyntax>())
                {
                    GraphConcept concept = createConceptFromExpressionSyntax(expr, conditionalExpression.Name);
                    if (concept != null)
                    {
                        Contains c1 = new Contains(conditionalExpression.Name, concept.Name);
                        this.addVerticesAndEdge(c1, concept);
                        this.addVerticesAndEdge(conditionalExpression, c1);
                    }
                }

                return conditionalExpression;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + ex.StackTrace);
            }

            return null;
        }

        private GraphConcept createConceptFromExpressionSyntax(ExpressionSyntax child, String currentScope)
        {
            try
            {
                if (child.GetType() == typeof(LiteralExpressionSyntax))
                {
                    StringDef term = new StringDef(((LiteralExpressionSyntax)child).Token.ToString(), ((LiteralExpressionSyntax)child).GetText().ToString());
                    return term;
                }

                if (child.GetType() == typeof(BinaryExpressionSyntax))
                {
                    MathOp mathop = createMathOpFromBinaryExpression((BinaryExpressionSyntax)child, currentScope);

                    return mathop;
                }

                if (child.GetType() == typeof(InvocationExpressionSyntax))
                {
                    // creates a MethodCall concept
                    MethodCall method = createMethodCallFromInvocationExpression((InvocationExpressionSyntax)child, currentScope);

                    return method;
                }

                if (child.GetType() == typeof(PrefixUnaryExpressionSyntax) || child.GetType() == typeof(PostfixUnaryExpressionSyntax))
                {
                    MathOp mathop;
                    if (child.GetType() == typeof(PrefixUnaryExpressionSyntax))
                        mathop = new MathOp(((PrefixUnaryExpressionSyntax)child).OperatorToken.ToString());
                    else
                        mathop = new MathOp(((PostfixUnaryExpressionSyntax)child).OperatorToken.ToString());// edited by Felicia 5.28.2014 from prefix to postfix

                    var id = child.ChildNodes().OfType<IdentifierNameSyntax>().Single();
                    GraphConcept concept = searchIdentifierInScope(id, currentScope);

                    if (concept != null)
                    {
                        Contains c = new Contains(mathop.Name, concept.Name);
                        this.addVerticesAndEdge(c, concept);
                        this.addVerticesAndEdge(mathop, c);
                    }
                    else
                    {
                        // define a null concept. an error has occured as no such variable or field was previously defined
                        NullDef term = new NullDef();
                        Contains c = new Contains(mathop.Name, term.Name);
                        this.addVerticesAndEdge(c, term);
                        this.addVerticesAndEdge(mathop, c);
                    }

                    return mathop;
                }

                if (child.GetType() == typeof(IdentifierNameSyntax))
                {
                    GraphConcept concept = searchIdentifierInScope((IdentifierNameSyntax)child, currentScope);
                    return concept;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + ex.StackTrace);
            }

            return null;
        }

        private SyntaxNode getSyntaxNodeFromParenthesis(SyntaxNode node)
        {
            SyntaxNode c = null;

            if (node.GetType() == typeof(CastExpressionSyntax))
                c = node.ChildNodes().OfType<SyntaxNode>().Except<SyntaxNode>(node.ChildNodes().OfType<PredefinedTypeSyntax>().Union<SyntaxNode>(node.ChildNodes().OfType<IdentifierNameSyntax>())).ElementAt(0);
            else
                c = node.ChildNodes().OfType<SyntaxNode>().Except<SyntaxNode>(node.ChildNodes().OfType<PredefinedTypeSyntax>()).ElementAt(0);


            while (c.GetType() == typeof(ParenthesizedExpressionSyntax) || c.GetType() == typeof(PrefixUnaryExpressionSyntax)
                || c.GetType() == typeof(CastExpressionSyntax))
            {
                if (c.GetType() == typeof(CastExpressionSyntax))
                    c = c.ChildNodes().OfType<SyntaxNode>().Except<SyntaxNode>(c.ChildNodes().OfType<PredefinedTypeSyntax>().Union<SyntaxNode>(c.ChildNodes().OfType<IdentifierNameSyntax>())).ElementAt(0);
                else
                    c = c.ChildNodes().OfType<SyntaxNode>().Except<SyntaxNode>(c.ChildNodes().OfType<PredefinedTypeSyntax>()).ElementAt(0);
            }

            return c;
        }

        // analyzes an EqualsValueClauseSyntax construct and adds the corresponding vertices and edges
        private AssignOp createAssignOpFromBinaryExpression(BinaryExpressionSyntax expression, String currentScope)
        {
            AssignOp assign = new AssignOp();
            assign.Value = expression.ToString();

            try
            {
                var childs = expression.ChildNodes().OfType<SyntaxNode>().ToList();
                List<SyntaxNode> adds = new List<SyntaxNode>();
                foreach (var c in childs)
                {
                    if (c.GetType() == typeof(ParenthesizedExpressionSyntax) || c.GetType() == typeof(PrefixUnaryExpressionSyntax)
                        || c.GetType() == typeof(CastExpressionSyntax))
                    {
                        adds.Add(getSyntaxNodeFromParenthesis(c));
                    }
                    else
                        if (c.GetType() == typeof(MemberAccessExpressionSyntax))
                    {
                        var elem = c.ChildNodes().OfType<IdentifierNameSyntax>();
                        if (elem.Count() > 0)
                            adds.Add(elem.ElementAt(0));
                    }
                }

                childs.AddRange(adds);

                foreach (var child in childs) //expression.DescendantNodes().OfType<SyntaxNode>().Except<SyntaxNode>(expression.DescendantNodes().OfType<SyntaxNode>().Where(d => d.GetType() == typeof(ParenthesizedExpressionSyntax) || d.GetType() == typeof(PrefixUnaryExpressionSyntax))))
                {
                    if (child.GetType() == typeof(BinaryExpressionSyntax))
                    {
                        MathOp mathop = createMathOpFromBinaryExpression((BinaryExpressionSyntax)child, currentScope);

                        Contains c = new Contains(assign.Name, mathop.Name);
                        this.addVerticesAndEdge(c, mathop);
                        this.addVerticesAndEdge(assign, c);
                    }
                    else
                        if (child.GetType() == typeof(LiteralExpressionSyntax))
                    {
                        StringDef term = new StringDef(((LiteralExpressionSyntax)child).Token.ToString(), ((LiteralExpressionSyntax)child).GetText().ToString());
                        Contains c = new Contains(assign.Name, term.Name);
                        this.addVerticesAndEdge(c, term);
                        this.addVerticesAndEdge(assign, c);
                    }
                    else
                            if (child.GetType() == typeof(IdentifierNameSyntax))
                    {
                        GraphConcept concept = searchIdentifierInScope((IdentifierNameSyntax)child, currentScope);
                        if (concept != null)
                        {
                            Contains c = new Contains(assign.Name, concept.Name);
                            this.addVerticesAndEdge(c, concept);
                            this.addVerticesAndEdge(assign, c);
                        }
                        else
                        {
                            // define a null concept. an error has occured as no such variable or field was previously defined
                            NullDef term = new NullDef();
                            Contains c = new Contains(assign.Name, term.Name);
                            this.addVerticesAndEdge(c, term);
                            this.addVerticesAndEdge(assign, c);
                        }
                    }
                    else
                                if (child.GetType() == typeof(InvocationExpressionSyntax))
                    {
                        // creates a MethodCall concept
                        MethodCall method = createMethodCallFromInvocationExpression((InvocationExpressionSyntax)child, currentScope);
                        Contains c = new Contains(assign.Name, method.Name);
                        this.addVerticesAndEdge(c, method);
                        this.addVerticesAndEdge(assign, c);
                    }
                }

                return assign;

            }
            catch (Exception ex)
            {
                //return null;
                throw ex;
            }
        }

    }
}
