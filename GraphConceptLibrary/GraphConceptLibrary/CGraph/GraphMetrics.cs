﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * Author: Daniela Grijincu 
 * daniela.grijincu.2013@nuim.ie
 * 
 * */
namespace GraphConceptsLibrary.CGraph
{
    class GraphMetrics
    {
        public static double AverageDegree(ConceptualGraph graph)
        {
            return (double)(2 * graph.EdgeCount) / (double)graph.VertexCount;
        }

        public static double ClusteringCoefficient(ConceptualGraph graph)
        {
            calculateVerticesClusteringCoefficient(graph);

            //A graph’s average clustering coefficient is the average clustering coefficient over all the nodes.
            double sum = 0;

            foreach (var v in graph.Vertices)
                sum += v.ClusteringCoefficient;


            return sum / graph.Vertices.Count();

        }

        private static void calculateVerticesClusteringCoefficient(ConceptualGraph graph)
        {
            foreach (var v in graph.Vertices)
            {
                var neighbours = graph.InEdges(v).Select(a => a.Source).Union(graph.OutEdges(v).Select(a => a.Target));

                int count = 0;

                if (neighbours != null && neighbours.Count() >= 2)
                {
                    // count the number of existing edges between the neighbours of v
                    foreach (var n in neighbours)
                    {
                        var currentNeighbours = graph.InEdges(n).Select(a => a.Source).Union(graph.OutEdges(n).Select(a => a.Target));

                        foreach (var other in neighbours)
                        {
                            if (n != other)
                            {
                                if (currentNeighbours.Contains(other))
                                    count++;
                            }
                        }
                    }

                    v.ClusteringCoefficient = (2 * count) / (neighbours.Count() * (neighbours.Count() - 1));

                }
                else
                    v.ClusteringCoefficient = 0;


            }
        }

        private static double QuadraticError = 0.001;
        private static double dampingFactor = 0.85;
        private static int IterationLimit = 50;

        private static double getConvergence(ConceptualGraph graph)
        {
            double sum = 0.0;
            foreach (var v in graph.Vertices)
            {
                sum += v.NodeRank;
            }

            //   Console.WriteLine("Sum = " + sum);

            return sum;
        }

        private static void normalizeValues(ConceptualGraph graph)
        {
            double min = 1.0;
            double max = 0.0;

            //calculate min and max
            foreach (var v in graph.Vertices)
            {
                if (v.NodeRank < min)
                    min = v.NodeRank;
                else
                    if (v.NodeRank > max)
                    max = v.NodeRank;
            }

            // normalize values
            foreach (var v in graph.Vertices)
            {
                v.NodeRank = (double)(v.NodeRank - min) / (double)(max - min);
            }

        }

        private static void normalizeValuesToOne(ConceptualGraph graph)
        {
            double sum = 0.0;
            foreach (var v in graph.Vertices)
            {
                sum += v.NodeRank;
            }

            // normalize values
            foreach (var v in graph.Vertices)
            {
                v.NodeRank = v.NodeRank / sum;
            }

        }

        public static void calculateActualNodeRanks(ConceptualGraph graph)
        {
            assignInitialNodeRanks(graph);

            int iteration = 0;
            double last = Double.MaxValue;

            while ((double)(last - getConvergence(graph)) > QuadraticError && iteration < IterationLimit)
            {
                last = getConvergence(graph);

                foreach (var u in graph.Vertices)
                {
                    //NR(u) = SUM (NR(v) / OutDegree(v)) // foreach v in IN(u)
                    var IN = graph.InEdges(u).Select(a => a.Source);
                    double sum = 0.0;
                    foreach (var v in IN)
                    {
                        sum += (v.NodeRank / graph.OutEdges(v).Count());
                    }

                    u.NodeRank = (((double)1 - dampingFactor) / (double)graph.Vertices.Count()) + dampingFactor * sum;
                    //     u.NodeRank =  sum;
                }

                normalizeValues(graph);
                //    normalizeValuesToOne(graph);

                // normalize values
                //foreach (var v in this.Vertices)
                //{
                //    v.NodeRank = (((double)1 - dampingFactor) / (double)this.Vertices.Count()) + dampingFactor * v.NodeRank;
                //}

                iteration++;
            }

            // the values have converged or the iteration limit has been exceeded
            graph.ConvergeValue = getConvergence(graph);
        }

        public static void assignInitialNodeRanks(ConceptualGraph graph)
        {
            foreach (var v in graph.Vertices)
                v.NodeRank = (double)((double)1 / (double)graph.Vertices.Count());
        }

    }
}
