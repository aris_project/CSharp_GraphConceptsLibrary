﻿using GraphConceptsLibrary.CGraph.Concepts;
using GraphConceptsLibrary.CGraph.Relations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * Author: Daniela Grijincu 
 * daniela.grijincu.2013@nuim.ie
 * 
 * */
namespace GraphConceptsLibrary.CGraph
{
    public abstract class GraphConcept : IComparable
    {
        public bool IsRelation { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }

        public double ClusteringCoefficient { get; set; }

        public double NodeRank { get; set; }
        public double ShortNodeRank
        {
            get
            {
                return Math.Round(NodeRank, 5);
            }
        }

        public bool IsClassDef
        {
            get
            {
                return this.GetType() == typeof(ClassDef);
            }
        }

        public bool IsBlock
        {
            get
            {
                return this.GetType() == typeof(Block);
            }
        }

        public bool IsLoop
        {
            get
            {
                return this.GetType() == typeof(Loop);
            }
        }

        public bool IsCompareOp
        {
            get
            {
                return this.GetType() == typeof(CompareOp);
            }
        }

        public bool IsAssignOp
        {
            get
            {
                return this.GetType() == typeof(AssignOp);
            }
        }

        public bool IsField
        {
            get
            {
                return this.GetType() == typeof(Field);
            }
        }

        public bool IsIf
        {
            get
            {
                return this.GetType() == typeof(If);
            }
        }

        public bool IsLogicalOp
        {
            get
            {
                return this.GetType() == typeof(LogicalOp);
            }
        }

        public bool IsMathOp
        {
            get
            {
                return this.GetType() == typeof(MathOp);
            }
        }

        public bool IsMethod
        {
            get
            {
                return this.GetType() == typeof(Method);
            }
        }

        public bool IsStringDef
        {
            get
            {
                return this.GetType() == typeof(StringDef);
            }
        }

        public bool IsVariable
        {
            get
            {
                return this.GetType() == typeof(Variable);
            }
        }

        public bool IsCondition
        {
            get
            {
                return this.GetType() == typeof(Condition);
            }
        }

        public bool IsContains
        {
            get
            {
                return this.GetType() == typeof(Contains);
            }
        }

        public bool IsDefines
        {
            get
            {
                return this.GetType() == typeof(Defines);
            }
        }

        public bool IsDepends
        {
            get
            {
                return this.GetType() == typeof(Depends);
            }
        }

        public bool IsParameter
        {
            get
            {
                return this.GetType() == typeof(Parameter);
            }
        }

        public bool IsReturns
        {
            get
            {
                return this.GetType() == typeof(Returns);
            }
        }

        public bool IsTryStatement
        {
            get
            {
                return this.GetType() == typeof(TryStatement);
            }
        }

        public GraphConcept() { Value = " "; }

        public GraphConcept(bool isrelation)
        {
            IsRelation = isrelation;
            Value = " ";
        }

        public GraphConcept(String name, bool isrelation)
        {
            Name = name;
            IsRelation = isrelation;
            Value = " ";
        }
        public GraphConcept(String name, String value, bool isrelation)
        {
            Name = name;
            Value = value;
            IsRelation = isrelation;
        }

        public int CompareTo(object obj)
        {
            if (((GraphConcept)obj).Name == this.Name)
                return 0;
            else return -1;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
